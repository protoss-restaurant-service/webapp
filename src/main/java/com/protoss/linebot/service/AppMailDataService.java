package com.protoss.linebot.service;

import org.springframework.http.ResponseEntity;

public interface AppMailDataService {

    ResponseEntity<String> saveByJsonCus(String json);

}
