package com.protoss.linebot.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppMailServiceImp implements AppMailDataService {

    public static List<String> LIST_KEYWORD = new ArrayList<>();
    public static List<String> LIST_PROGRAM = new ArrayList<>();

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Value("${engine.url}")
    private String ENGINE_URL;

    @Override
    public ResponseEntity<String> saveByJsonCus(String json) {
        LOGGER.info("{}",json);
        RestTemplate restTemplate = new RestTemplate();
        String url = ENGINE_URL.concat("/Dashboard/saveByJsonCus");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<String>(json, headers);
        LOGGER.info("request :{}", url);
        try {
            return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static boolean checkTextMatches(String str) {
        int countKeyword = 0;
        int countProgram = 0;

        for (String keyword : LIST_KEYWORD) {
            if (str.indexOf(keyword) >= 0) {
                countKeyword++;
            }
        }
        for (String keyword : LIST_PROGRAM) {
            if (str.indexOf(keyword) >= 0) {
                countProgram++;
                break;
            }
        }
        if (countKeyword != 0 && countProgram != 0) {
            return true;

        } else {
            return false;
        }
    }



}
